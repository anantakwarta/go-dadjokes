package main

import (
	"encoding/json"
	"fmt"

	"github.com/gofiber/fiber/v2"
)

type DadJoke struct {
	ID     string `json:"id"`
	Joke   string `json:"joke"`
	Status int    `json:"status"`
}

func main() {
	app := fiber.New()

	app.Get("/getDadJoke", getDadJoke)

	app.Listen(":3000")
}

func getDadJoke(c *fiber.Ctx) error {
	var apiResp DadJoke
	agent := fiber.Get("https://icanhazdadjoke.com").Set("Accept", "application/json")
	if err := agent.Parse(); err != nil {
		panic(err)
	}
	agent.Struct(&apiResp)
	
	response, err := json.Marshal(apiResp)
	fmt.Println(apiResp, string(response))
	if err != nil {
		panic(err)
	}
	return c.SendString(string(response))

}
